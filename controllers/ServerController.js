// Task contoller

// import Task model
require("dotenv").config();
require("../config/db").connect();
const ping = require("ping");
// import nodemailer
const nodemailer = require("nodemailer");

const cron = require('node-cron');



// server status
const serverStatus = async (req, res) => {

 const mailServer = await ping.promise.probe('samuel-kago.tech');

 const Webserver = await ping.promise.probe('41.79.9.117');

 const Webserver1 = await ping.promise.probe('194.201.253.8')

  const data = [
    
      {
        host: mailServer.host,
        alive: mailServer.alive,
        output: mailServer.output,
        time: mailServer.time,
        min: mailServer.min,
        max: mailServer.max,
        avg: mailServer.avg,
        stddev: mailServer.stddev,
        packetLoss: mailServer.packetLoss,
        results: mailServer.results
      },
      {
        host: Webserver1.host,
        alive: Webserver1.alive,
        output: Webserver1.output,
        time: Webserver1.time,
        min: Webserver1.min,
        max: Webserver1.max,
        avg: Webserver1.avg,
        stddev: Webserver1.stddev,
        packetLoss: Webserver1.packetLoss,
        results: Webserver1.results
      },
      {
        host: Webserver.host,
        alive: Webserver.alive,
        output: Webserver.output,
        time: Webserver.time,
        min: Webserver.min,
        max: Webserver.max,
        avg: Webserver.avg,
        stddev: Webserver.stddev,
        packetLoss: Webserver.packetLoss,
        results: Webserver.results
      }

    ]
  

  res.status(200).json({
    status: "success",
    data,
  });
};






      

// send mail notification at 6:00 am
const sendEmail = async (req, res) => {

  
  // ping 
 const server = await ping.promise.probe('samuel-kago.tech');


 const data = {
    host: server.host,
    alive: server.alive,
    output: server.output,
    time: server.time,
    min: server.min,
    max: server.max,
    avg: server.avg,
    stddev: server.stddev,
    packetLoss: server.packetLoss,
    results: server.results

 }



    
  let transporter = nodemailer.createTransport({
    host: "us2.smtp.mailhostbox.com",
    port: 587,
    secure: false, // true for 465, false for other ports
    auth: {
      user: "no-reply@samuel-kago.tech", // generated ethereal user
      pass: "!PyUHhCf5" // generated ethereal password
    },
  });


    const mailOptions = {
        from: "no-reply@samuel-kago.tech",
        to: "samuel.kago@kenyaweb.com",
        subject: "Server Status",
        text: JSON.stringify(data),

    };

    transporter.sendMail(mailOptions, (err, data) => {

        if (err) {
            res.status(500).json({
                message: "Internal server error"
            });
        } else {
            res.status(200).json({
                message: "Server Status",
                data:data
            });
        }
    });


};


// set up cron job to send mail at 6:00 am

// Schedule tasks to be run on the server.
cron.schedule('*/10 * * * *', function() {
  // send mail at 6:00 am
  sendEmail();
  console.log('running a task every minute');
});



module.exports = {
    sendEmail,
    serverStatus
}
