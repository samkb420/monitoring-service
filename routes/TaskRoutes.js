// router

const express = require("express");

const router = express.Router();

const { serverStatus } = require("../controllers/ServerController");


// server status

router.get("/server-status", serverStatus);



module.exports = {
    routes:router
}


