// method to send mail at 6:00 am using nodemailer

const cron = require('node-cron');

// ...

// Schedule tasks to be run on the server.
cron.schedule('* * * * *', function() {
    // send mail at 6:00 am
    sendEmail();
    console.log('running a task every minute');
  });

// ...


